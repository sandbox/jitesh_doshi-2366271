<?php

function _appointments_timeslot_options($component, $flat, $arguments) {
  $slot = isset($component['extra']['timeslot'])
    ? $component['extra']['timeslot']
    : $arguments;
  $options = array();
  $first = (int)$slot['first'];
  $last = (int)$slot['last'];
  for($i = $first; $i <= $last; $i++) {
    if($i < 12) {
      $label = sprintf("%02d:00 AM", $i);
    } else if ($i == 12) {
      $label = 'noon';
    } else {
      $label = sprintf("%02d:00 PM", ($i-12));
    }
    $key = (string)$i;
    $options[$key] = $label;
  }
  return $options;
}
